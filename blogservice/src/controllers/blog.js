const { object } = require("joi");
const useCases = require("../use-cases");

module.exports = ({db, context}, blogUseCase)=>{
    return Object.freeze({
        add,
        update,
        del,
        getAll,
        getSingle
    });

    async function add(req, res){
        console.log(`controller->add context.id=${context().reqId}`);
        console.log(`body = ${JSON.stringify(req.body, null,2)}`);
        console.log(`params = ${JSON.stringify(req.params, null,2)}`);
        console.log(`query = ${JSON.stringify(req.query, null,2)}`);
        console.log(`header = ${JSON.stringify(req.headers, null,2)}`);
      
        //TODO: get a valid blog entity
        let blog = blogUseCase.add(req, res);
        console.log(`after use case transformation blog = ${JSON.stringify(blog, null,2)}`);
        await db.add(blog);
    }

    async function update(req, res){
        console.log(`controller->update context.id=${context().reqId}`);
        console.log(`body = ${JSON.stringify(req.body, null,2)}`);
        console.log(`params = ${JSON.stringify(req.params, null,2)}`);
        console.log(`query = ${JSON.stringify(req.query, null,2)}`);
        console.log(`header = ${JSON.stringify(req.headers, null,2)}`);
        blogUseCase.update(req, res);
        await db.update({});
    }

    async function del(req, res,){
        console.log(`controller->del context.id=${context().reqId}`);
        console.log(`body = ${JSON.stringify(req.body, null,2)}`);
        console.log(`params = ${JSON.stringify(req.params, null,2)}`);
        console.log(`query = ${JSON.stringify(req.query, null,2)}`);
        console.log(`header = ${JSON.stringify(req.headers, null,2)}`);
        blogUseCase.del(req, res);
        await db.del({});
    }

    async function getAll(req, res){
        console.log(`controller->getAll context.id=${context().reqId}`);
        console.log(`body = ${JSON.stringify(req.body, null,2)}`);
        console.log(`params = ${JSON.stringify(req.params, null,2)}`);
        console.log(`query = ${JSON.stringify(req.query, null,2)}`);
        console.log(`header = ${JSON.stringify(req.headers, null,2)}`);
        blogUseCase.getAll(req, res);
        await db.getAll({});
    }

    async function getSingle(req, res){
        console.log(`controller->getSingle context.reqId=${context().reqId}`);
        console.log(`body = ${JSON.stringify(req.body, null,2)}`);
        console.log(`params = ${JSON.stringify(req.params, null,2)}`);
        console.log(`query = ${JSON.stringify(req.query, null,2)}`);
        console.log(`header = ${JSON.stringify(req.headers, null,2)}`);
        blogUseCase.getSingle(req, res);
        await db.getSingle({});
    }
}