module.exports = (blogController)=>{
    let getSingleBlogRouter = require('express').Router();
    getSingleBlogRouter.get('/:id', async (req, res)=>{
        await blogController.getSingle(req, res);
        res.status(200).send("get single blog by id blog done");
    });

    return getSingleBlogRouter;
}