module.exports = (blogController)=>{
    let getBlogRouter = require('express').Router();
    getBlogRouter.get('/', async (req, res)=>{
        await blogController.getAll(req, res);
        res.status(200).send("get all blogs done");
    });

    return getBlogRouter;
}