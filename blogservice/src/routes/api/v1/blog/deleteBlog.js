module.exports = (blogController)=>{
    let getDeleteBlogRouter = require('express').Router();
    getDeleteBlogRouter.delete('/:id/', async (req, res)=>{
        await blogController.del(req, res);
        res.status(200).send("delete single blog by id blog done");
    });

    return getDeleteBlogRouter;
}