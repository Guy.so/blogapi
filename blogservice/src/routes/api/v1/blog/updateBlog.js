module.exports = (blogController)=>{
    let updateBlogRouter = require('express').Router();
    updateBlogRouter.put('/', async (req, res)=>{
        await blogController.updateBlogRouter(req, res);
        res.status(200).send("update blog done");
    });

    return updateBlogRouter;
}