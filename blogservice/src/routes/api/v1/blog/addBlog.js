let routeHandler = require('../../routeHandler');

module.exports = (blogController)=>{
    var express = require('express');
    let addBlogRouter = require('express').Router();
    
    addBlogRouter.post('/', async (req, res)=>{
        await routeHandler(req, res, blogController.add);
    });

    return addBlogRouter;
}