const addBlog = require('./addBlog');
const updateBlog = require('./updateBlog');
const deleteBlog = require('./deleteBlog');
const getBlog = require('./getBlog');
const getSingleBlog = require('./getSingleBlog');

module.exports = (controller)=>{
    return [
        addBlog(controller),
        updateBlog(controller),
        deleteBlog(controller),
        getBlog(controller),
        getSingleBlog(controller)
    ]
}
