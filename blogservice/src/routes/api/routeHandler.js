const uuid = require('uuid');
module.exports = async (req, res, controllerFunc)=>{
    let context = {};
    context.reqId = "123";
    try {
        console.log("in router wrapper");
        context.body = req.body;
        let result = await controllerFunc(req, res, context);
        res.status(200).send(result);

    } catch (error) {
        error.reqId = context.reqId;
        res.status(501).send(error);
    }
}