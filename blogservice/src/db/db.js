module.exports = ({config})=>{

    //TODO: connect to the db here using the config details 

    return {
        add,
        update,
        del,
        getAll,
        getSingle
    }

    async function add(blog){
        console.log("from db->add");
        console.log(`blog to add is =  ${JSON.stringify(blog, null, 2)}`);
    }

    async function update(blog){
        console.log("from db->update");
    }

    async function del(blog){
        console.log("from db->delete");
    }

    async function getAll(blog){
        console.log("from db->getAll");
    }

    async function getSingle(blog){
        console.log("from db->getSingle");
    }

}