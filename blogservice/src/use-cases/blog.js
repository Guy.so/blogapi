let dataModel = require('./../model/blog');
module.exports = (services)=>{
    return Object.freeze({
        add,
        update,
        del,
        getAll,
        getSingle
    });

    function add(req, res){
        console.log(`useCase->add context.id=${services.context().reqId}`);
        let blog = dataModel(services)(req.body);
        console.log(`useCase->blog = ${JSON.stringify(blog, null, 2)}`);
       
        return blog;
    }

    async function update(req, res){
        console.log(`useCase->update context.id=${services.context().reqId}`);
    }

    async function del(req, res){
        console.log(`useCase->del context.id=${services.context().reqId}`);
    }

    async function getAll(req, res){
        console.log(`useCase->getAll context.id=${services.context().reqId}`);
    }

    async function getSingle(req, res){
        console.log(`useCase->getSingle context.id=${services.context().reqId}`);
        return "done";
    }
}