module.exports = log = {
    "info": (data)=>{
        return `info: ${data}`;
    },
    "warn": (data)=>{
        return `warn: ${data}`;
    },
    "err": (data)=>{
        return `err: ${data}`;
    },
    "trace": (data)=>{
        return `trace: ${data}`;
    }
}