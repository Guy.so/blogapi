const PORT = 1234;
const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');
let blogRoutes = require('./src/routes/api/v1/blog');
let controllers = require('./src/controllers');
let useCases = require('./src/use-cases');
const data = {};
const db = require('./src/db/db');

let app = express();
app.server = http.createServer(app);

const config = {"connection": ""};

const services = {
    db: db(config),
    context: ()=> {
        return {
            "reqId": "1234567"
        }
    },
    isTextModerateValid: (text)=>{
        console.log(`call moderate service for checking the text ${text}`)
        return true;
    }
}

app.use(bodyParser.json({limit: "1024Mb"}));
app.use('/api/v1/blogs', 
blogRoutes(
    controllers.blog(services, 
        useCases.blog(services))));

app.listen(process.env.PORT || PORT, ()=>{
    console.log(`Server is listning on port ${PORT}`);
});