# blogapi

****Clean Architecture implementation for**: 
Blog rest API example, 
using Node.js express server & ReactJS client**.


**other references I use**: 

    By Robert C. Martin (Uncle Bob), books & articles,

        https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html

    By Khalil Stemmler, publish on: khalilstemmler.com,
        https://khalilstemmler.com/articles/enterprise-typescript-nodejs/clean-nodejs-architecture/
        
        
    By author, Royi: 
        https://medium.com/better-programming/node-clean-architecture-deep-dive-ab68e523554b
        
        
    
*Being tested*: 


**create**:
POST: http://localhost:1234/api/v1/blogs/


req.body = {
    "title": "blog title",
    "id": "",
    "author": "guy",
    "blogId": "123",
    "desc": "blog desc",
    "tags": "vle an architecture node.js"
}


**GET**: http://localhost:1234/api/v1/blogs/1/
req.params.id = 1


*TBD tested: other methods*



